-- Author: Astrobe
-- Version: 0.1
-- License: LGPL 2.1
--
-- Dokimi's self-organizing systems with its ever-expanding poison gas inspired this less deadly smoke mod.

local ITVL=2.5 -- how fast smoke moves, in seconds. Smoke is generate at twice this period (so half slower than the smoke moves).
local PERSIST=0.93 -- the chance smokes has *not* to dissipate. Very sensitive parameter: in terms of average smoke column average half-height, 0.95 -> 14, 0.93 -> 10 but 0.80 -> 5
local SOURCES={"default:furnace_active", "fire:basic_flame", "default:lava_source"}


local sb={name="smoke:block"} -- avoid building that table each time our wannabe fast function is executed.
local ab={name="air"}

local function remove_smoke(pos)
	if minetest.get_node(pos).name=="smoke:block" then
		minetest.swap_node(pos, ab)
	end
end



--[[ This is the simpler, slightly faster version but it does not randomize the smoke's walk and let it escape to diagonal nodes.
-- Also it relies on the fact that *currently* looks at the node above first. The docs don't make any statement about in which order the neighbors are checked.
local function spread_smoke(pos)
	local p={x=pos.x, y=pos.y+1, z=pos.z}
	if minetest.get_node(p).name=="IGNORE" then remove_smoke(pos) end
	p=minetest.find_node_near(pos, 1, "air")
	if p then
		remove_smoke(pos) 
		if math.random()<PERSIST then
			minetest.set_node(p, sb)
		end
	else
		-- trapped; retry later
		minetest.get_node_timer(pos):start(ITVL*2)
	end
end
--]]

local locations = {
	1, 0,  -1, 0,  0, 1,  0, -1,
	1, 0,  -1, 0,  0, 1,  0, -1,
}

-- This function does everything it can to be as fast as possible, all while randomzing the movement of the nodes.
-- The "locations" aboves are pairs of x,z coordinates for the four horizontal neighbors (no diagonals),
-- repeated two times.
-- spread_smoke picks at random a starting pair in this array. It adds the offsets to the coordinates,
-- check if the node there is air, and if not subtracts the current offsets and adds the next ones in one go.
-- This gives 4 different check orders out of the 4! = 24 possible in theory. Of course one could write down all 24 orders in an array...

local function spread_smoke(pos)
	local p={x=pos.x, y=pos.y+1, z=pos.z}
	local gn=minetest.get_node
	local r=math.random(0, 3)*2+1
	local loc=locations

	repeat
		if gn(p).name=="air" then break end
		if gn(p).name=="ignore" then remove_smoke(pos) return false end -- we don't want wandering smoke nodes at the mapblock limit in the sky.
		p.y=p.y-1
		p.x=p.x+loc[r]
		p.z=p.z+loc[r+1]
		if gn(p).name=="air" then break end
		p.x=p.x-loc[r]+loc[r+2]
		p.z=p.z-loc[r+1]+loc[r+3]
		if gn(p).name=="air" then break end
		p.x=p.x-loc[r+2]+loc[r+4]
		p.z=p.z-loc[r+3]+loc[r+5]
		if gn(p).name=="air" then break end
		p.x=p.x-loc[r+4]+loc[r+6]
		p.z=p.z-loc[r+5]+loc[r+7]
		if gn(p).name=="air" then break end
		p.x=p.x-loc[r+6]
		p.z=p.z-loc[r+7]
		p.y=p.y-1
		if gn(p).name=="air" then break end
		-- trapped; retry later
		minetest.get_node_timer(pos):start(ITVL*2)
		return false
	until true -- never loop; all we wanted is a freaking Goto.

	if math.random()<PERSIST then
		minetest.set_node(p, sb)
	end
	remove_smoke(pos)
	return false
end
--]]

--[[
local function spread_smoke(pos)
	local p={x=pos.x, y=pos.y+1, z=pos.z}
	local node=minetest.get_node_or_nil(p)
	if not node then remove_smoke(pos) return end -- reached the mapblock limit. We don't want accumulated smokes forever in the sky.
	-- This is terrible, but it avoids creating a coordinates table for each position to be checked.
	-- Could be done slighty more cleanly in a function, but the main theme is execution speed here.
	local gn=minetest.get_node
	repeat
		if gn(p).name=="air" then break end
		p.y=p.y-1
		p.x=p.x+1
		if gn(p).name=="air" then break end
		p.x=p.x-1
		p.z=p.z+1
		if gn(p).name=="air" then break end
		p.z=p.z-2 -- sic, this tests pos.z-1
		if gn(p).name=="air" then break end
		p.z=p.z+1
		p.x=p.x-1
		if gn(p).name=="air" then break end
		p.x=p.x+1
		p.y=p.y-1
		if gn(p).name=="air" and math.random(1, DOWN)==1 then break end
		p=nil
	until true -- never loop; all we wanted is a freaking Goto.

	if p then
		remove_smoke(pos)
		if math.random(1,PERSIST)~=1 then
			minetest.set_node(p, {name="smoke:block"})
		end
	else
		-- trapped, most likely by other smoke nodes: retry later
		minetest.get_node_timer(pos):start(ITVL*2)
	end
end
--]]

minetest.register_node('smoke:block', {
	description = 'Smoke',
	tiles = {{
		name = "smoke.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 6,
		},
	}},
	inventory_image = "smoke.png^[verticalframe:16:1",
	wield_image =  "smoke.png^[verticalframe:16:1",
	drawtype = "glasslike",
	paramtype = "light",
	use_texture_alpha=true,
	sunlight_propagates = true,
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	damage_per_second = 1,
	drowning=1,
	post_effect_color = {a = 200, r = 177, g = 177, b = 177},
	drop="",

	on_timer=spread_smoke,
	on_construct=function(pos)
		minetest.get_node_timer(pos):start(ITVL+math.random())
	end
	})

minetest.register_abm{
	label="Smoke",
	nodenames= SOURCES,
	neighbors={"air"},
	interval=ITVL*2,
	chance=1,
	action=spread_smoke
}
--]]
